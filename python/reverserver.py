#!/usr/bin/env python3

"""
Simple webserver that gets JSON on the endpoint / and returns the same JSON vack
with the text reversed.
"""

import json
from flask import Flask
from flask import request

APP = Flask(__name__)

@APP.route("/", methods=["POST"])
def reverse():
    """
    Route /, gets json {"text":"sometext"}
    and sends the same json back with the text reversed
    """
    to_reverse = request.get_json()
    reverse_request = to_reverse["text"][::-1]
    return json.dumps({"text":reverse_request})

if __name__ == "__main__":
    APP.run(debug=True, host="127.0.0.1", port=8080)
