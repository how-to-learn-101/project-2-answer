Run with

```
$ python reverserver.py
```

Must have Python 3 installed.
Must have Flask installed. If Flask isn't installed, grab the requirements
with pip:

```
$ pip install -r requirements.txt
```
