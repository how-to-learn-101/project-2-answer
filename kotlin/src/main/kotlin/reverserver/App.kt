package reverserver

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.gson.gson
import io.ktor.routing.post
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.routing
import io.ktor.routing.route
import io.ktor.features.ContentNegotiation
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

data class Reverser(val text: String)

fun main(args: Array<String>) {
    embeddedServer(Netty, 8080) {
        install(ContentNegotiation) { gson {} }
        routing {
            route("/") {
                post {
                    val text = call.receive<Reverser>()
                    call.respond(Reverser(text.text.reversed()))
                }
            }
        }
    }.start(wait = false)
}
