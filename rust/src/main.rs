extern crate actix_web;
#[macro_use] extern crate serde_derive;
use actix_web::{Json, http, server, App};

#[derive(Deserialize)]
#[derive(Serialize)]
struct ReverseObject {
    text: String
}

fn reverse(text: Json<ReverseObject>) -> Json<ReverseObject> {
    Json(ReverseObject{
        text: text.text.chars().rev().collect::<String>(),
    })
}

fn main() {
    println!("Starting webserver on port 8080");
    server::new(||App::new()
                .resource("/", |r| r.method(http::Method::POST).with(reverse)))
        .bind("127.0.0.1:8080")
        .unwrap()
        .run();

}
